package main

//Intentionally commented out for the following exercises
/*
import (
	"fmt"
)
*/

func main() {
	fmt.Println("Hello from our Collision from Home Workshop!")
	fmt.Println("Today we learn about GitLab Best Practices and CI/CD magic :)")

	fmt.Println("");

	fmt.Print(GetTanuki(true))

	fmt.Println("");
	fmt.Println("Join us at https://www.everyonecancontribute.com")
}
